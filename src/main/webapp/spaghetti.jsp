<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	int num = 0;
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%
	// 새 코드블럭을 무지성으로 아무곳에나 만들어서 위의 코드블럭에 있던 내용을 옮겨보았다.
	// 이렇게 코드가 늘어날 때마다 코드블럭이 여러곳에 산재하게 되어서 가독성이 나빠지고
	// 코드가 꼬일 수 있는 상황이 자연스럽게 많이 나올 수밖에 없다.
	// 물론 개발자가 매번 신경쓰며 코드를 작성하면 해결될 수 있지만, 사람일이라는게 그리 쉽게 되지 않으니까..
	// 특히 코드 길이가 길어지면 디버깅할 때 토나올 것 같다.
	try {
		num = Integer.parseInt(request.getParameter("num"));
	} catch (Exception ignore){}
%>
<body>
	<%if(num <= 0) {%>
	0보다 큰 정수를 입력해주세요.
	<%} else if(num%2 == 0) {%>
	짝수입니다.
	<%} else {%>
	홀수입니다.
	<%}%>
</body>
</html>