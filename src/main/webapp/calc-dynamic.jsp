<!-- 이 설정 문구를 넣어주어야 한글과 특수문자가 UTF-8로 정상적으로 java 기입되었다.
이 블럭은 코드블럭이라고 하지 않고 페이지 지시자 블럭이라고 한다. -->
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
<%!
	// 이런 멤버 함수같은 경우는 service 함수 내부에 선언되면 안 되기 때문에
	// service 함수 바깥에 코드를 기입하라는 의미로 <%!라는 코드블럭을 사용해야 한다.
	public int sum(int a, int b)
	{
		return a + b;
	}
%>
<%
	// 이 코드블럭의 코드는 service 함수 내부에 기입된다.
	int a = 4;
%>
<%
	// 이 쿠키에서 exp를 가져온다.
		Cookie[] cookies = request.getCookies();
		String exp = "0";
		
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if(cookie.getName().equals("exp")) {
					exp = cookie.getValue();
					break;
				}
			}
		}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
input {
	width:50px;
	height:50px;
}
.output {
	height: 50px;
	background: #e9e9e9;
	font-size:24px;
	font-weight: bold;
	text-align: right;
	padding:0px 5px;
}
</style>
</head>
<body>
	<form action="calc-dynamic" method="post">
		<table>
			<tr>
				<td class="output" colspan="4"><%=exp%></td>
			</tr>
			<tr>
				<td><input type="submit" name="op" value="CE" /></td>
				<td><input type="submit" name="op" value="C" /></td>
				<td><input type="submit" name="op" value="DEL" /></td>
				<td><input type="submit" name="op" value="÷" /></td>
			</tr>
			<tr>
				<td><input type="submit" name="value" value="7" /></td>
				<td><input type="submit" name="value" value="8" /></td>
				<td><input type="submit" name="value" value="9" /></td>
				<td><input type="submit" name="op" value="X" /></td>
			</tr>
			<tr>
				<td><input type="submit" name="value" value="4" /></td>
				<td><input type="submit" name="value" value="5" /></td>
				<td><input type="submit" name="value" value="6" /></td>
				<td><input type="submit" name="op" value="-" /></td>
			</tr>
			<tr>
				<td><input type="submit" name="value" value="1" /></td>
				<td><input type="submit" name="value" value="2" /></td>
				<td><input type="submit" name="value" value="3" /></td>
				<td><input type="submit" name="op" value="+" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="value" value="0" /></td>
				<td><input type="submit" name="dot" value="." /></td>
				<td><input type="submit" name="op" value="=" /></td>
			</tr>
		</table>
	</form>
</body>
</html>