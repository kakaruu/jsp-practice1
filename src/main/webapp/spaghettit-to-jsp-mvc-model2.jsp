<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!--
	JSP MVC Model 2
	JSP MVC Model 1에서 JSP 문서에 작성했던 컨트롤러를 별도의 java 파일로 분리하는 방법
	
	View 파트
	JSP 문서 하단에서는 상단에서 처리한 모델을 가지고 출력만 한다.
	-->
	<%=request.getAttribute("model")%>
	
	<br>
	<!--
	JSP에는 나중에 EL(Expression Language)라는 것이 추가되었는데
	아래처럼 Attribute를 손쉽게 꺼내서 출력하기도 하고 각종 연산식을 사용할 수 있는 기능이다.
	--> 
	${model}
	
	<br>
	<!--
	EL로 배열을 가져와서 0번째 항목을 출력
	-->
	<!--
	or가 예약어라 오류가 난다.
	어떻게 escape 해야하는 지 모르겠다..
	{array-or-list[0]} ?? 아니 왜 주석에 넣었는데도 에러가 나지..ㅋㅋ $빼주니까 해결됐다.
	-->
	${arrayOrList[0]}
	
	<br>
	<!--
	EL로 Map을 가져와서 key로 각 항목을 출력
	-->
	${map.key1} ${map.key2}
</body>
</html>