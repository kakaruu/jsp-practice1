<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	// JSP MVC Model 1
	// JSP의 코드블럭이 산재하는 문제를 해결하기 위해 처음 고안되었던 방식.
	
	// Controller 파트
	// JSP 문서 상단에 처리문을 넣어서 화면에 보여줘야하는 데이터들을 Model에다가 넣는다.
	int num = 0;
	try {
		num = Integer.parseInt(request.getParameter("num"));
	} catch (Exception ignore){}
	String model = "";
	if(num <= 0) {
		model = "0보다 큰 정수를 입력해주세요.";
	} else if(num%2 == 0) {
		model = "짝수입니다.";
	} else {
		model = "홀수입니다.";
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!--
	View 파트
	JSP 문서 하단에서는 상단에서 처리한 모델을 가지고 출력만 한다.
	-->
	<%=model%>
</body>
</html>