package com.practice.web.notice;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/notice/reg")
public class Reg extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8"); // 발신자가 어떻게 보내는지를 지정
		resp.setContentType("text/html; charset=UTF-8"); // 수신자에게 어떻게 처리하라고 지정
//		req.setCharacterEncoding("UTF-8"); // 요청으로 받은 내용을 UTF-8로 인코딩
		
		PrintWriter out = resp.getWriter();
		
		String title = req.getParameter("title");
		String content = req.getParameter("content");
		
		out.println("<h3>" + title + "</h3>");
		out.println(content);
	}
}
