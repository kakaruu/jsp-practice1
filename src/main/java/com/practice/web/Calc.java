package com.practice.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calc")
public class Calc extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8"); // 발신자가 어떻게 보내는지를 지정
		resp.setContentType("text/html; charset=UTF-8"); // 수신자에게 어떻게 처리하라고 지정

		int paramV = 0;
		String paramOp = req.getParameter("op");
		try {
			paramV = Integer.parseInt(req.getParameter("v"));
		} catch(Exception ignore) {}
		
		// Application Context
		// 사용범위: 전역 범위
		// 생명주기: WAS가 종료될 때까지 유지
		// 저장위치: WAS가 돌아가는 시스템의 메모리
		// 비고: 시스템 전역이 공유하기 때문에, 개개인의 정보를 여기에 담는 것은 미친 짓 
		
		// Application 전역 컨텍스트를 가져온다.
		ServletContext application = req.getServletContext();
		
		switch(paramOp) {
		case "=":
			try {
				int storedV = (Integer)application.getAttribute("value");
				String op = (String)application.getAttribute("op");
				
				int result = 0;
				if(op.equals("+")) {
					result = storedV + paramV;
				} else if (op.equals("-")) {
					result = storedV - paramV;
				} else {
					throw new Exception();
				}
				
				resp.getWriter().println("연산 결과는 " + result + " 입니다.");
			} catch(Exception ignore) {
				resp.getWriter().println("연산식이 올바르지 않습니다.");
			} finally {
				application.removeAttribute("value");
				application.removeAttribute("op");
			}
			break;
		case "+":
		case "-":
			application.setAttribute("value", paramV);
			application.setAttribute("op", paramOp);
			
			resp.sendRedirect("/calc.html");
			break;
		}
	}
}
