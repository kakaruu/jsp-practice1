package com.practice.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calc-page")
public class CalcPage extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Cookie[] cookies = req.getCookies();
		
		String exp = "0";
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if(cookie.getName().equals("exp")) {
					exp = cookie.getValue();
					break;
				}
			}
		}
		
		PrintWriter out = resp.getWriter();
		
		out.write("<!DOCTYPE html>\n" +
				"<html>\n" +
				"<head>\n" +
				"<meta charset=\"UTF-8\">\n" +
				"<title>Insert title here</title>\n" +
				"<style>\n" +
				"input {\n" +
				"	width:50px;\n" +
				"	height:50px;\n" +
				"}\n" +
				".output {\n" +
				"	height: 50px;\n" +
				"	background: #e9e9e9;\n" +
				"	font-size:24px;\n" +
				"	font-weight: bold;\n" +
				"	text-align: right;\n" +
				"	padding:0px 5px;\n" +
				"}\n" +
				"</style>\n" +
				"</head>\n" +
				"<body>\n" +
				"	<form action=\"calc-dynamic\" method=\"post\">\n" +
				"		<table>\n" +
				"			<tr>\n" +
				"				<td class=\"output\" colspan=\"4\">" + exp + "</td>\n" +
				"			</tr>\n" +
				"			<tr>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"CE\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"C\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"DEL\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"÷\" /></td>\n" +
				"			</tr>\n" +
				"			<tr>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"7\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"8\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"9\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"X\" /></td>\n" +
				"			</tr>\n" +
				"			<tr>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"4\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"5\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"6\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"-\" /></td>\n" +
				"			</tr>\n" +
				"			<tr>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"1\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"2\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"3\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"+\" /></td>\n" +
				"			</tr>\n" +
				"			<tr>\n" +
				"				<td></td>\n" +
				"				<td><input type=\"submit\" name=\"value\" value=\"0\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"dot\" value=\".\" /></td>\n" +
				"				<td><input type=\"submit\" name=\"op\" value=\"=\" /></td>\n" +
				"			</tr>\n" +
				"		</table>\n" +
				"	</form>\n" +
				"</body>\n" +
				"</html>\n");
	}
}
