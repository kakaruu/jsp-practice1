package com.practice.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/*")
public class CharacterEncodingFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request,
			ServletResponse response,
			FilterChain chain)
			throws IOException, ServletException {
		// 필터에서 아무것도 안 하고 끝내버리면 사용자에게 빈내용의 결과가 반환된다.
//		System.out.println("hello filter");
		
		// chain을 하기 전에 request의 내용을 UTF-8로 인코딩 처리
		// 필터에 걸리는 모든 req를 UTF-8로 처리할 수 있다.
		request.setCharacterEncoding("UTF-8");
		
		// chain을 하기 전에 response의 내용을 UTF-8로 처리하도록 설정
		// 발신자가 어떻게 보내는지를 지정
		// 이 부분을 필터에 넣으면 필터에 걸리는 모든 문서의 응답 인코딩이 UTF-8로 설정된다.
		// 응답은 UTF-8로 설정하는 경우가 대부분이기 때문에 큰 문제는 안 될듯하지만,
		// 각 문서마다 인코딩을 적용하는게 더 올바른 설정인 것 같긴 하다.
		response.setCharacterEncoding("UTF-8");
		
		// chain.doFiler를 통해 request와 response를 원래의 타겟에게 전달한다
		// 이를 전달하면 사용자에게 원래의 결과가 출력된다.
		chain.doFilter(request, response);
		
		// 수신자에게 어떻게 처리하라고 지정 이 부분을 필터에 넣으려면 경로를 잘 지정해주어야 한다.
		// html 형식이 아닌 문서도 있을 것이고, UTF-8로 제공되는 문서가 아닌 경우도 있기 때문
//		response.setContentType("text/html; charset=UTF-8");
	}

}
