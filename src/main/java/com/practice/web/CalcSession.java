package com.practice.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/calc-session")
public class CalcSession extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8"); // 발신자가 어떻게 보내는지를 지정
		resp.setContentType("text/html; charset=UTF-8"); // 수신자에게 어떻게 처리하라고 지정

		int paramV = 0;
		String paramOp = req.getParameter("op");
		try {
			paramV = Integer.parseInt(req.getParameter("v"));
		} catch(Exception ignore) {}
		
		// Session
		// 사용범위: 특정 세션 ID를 가진 사용자, 해당 세션에서 사용하는 저장 공간
		// 생명주기: 세션이 종료될 때까지(기본 30분)
		// 저장위치: WAS가 돌아가는 시스템의 메모리
		// 비고: 세션 ID를 다른 사용자가 훔쳐 사용할 수도 있음.
		// 세션 ID는 바뀌기 때문에 사용자마다 오래 유지되어야 하는 값을 저장하기에는 적합하지 않다.
		
		// 요청의 Session을 가져온다.
		HttpSession session = req.getSession();
		
		// 세션의 디폴트 유지시간은 30분이다.
//		session.setMaxInactiveInterval(300) // 세션의 유효시간을 5분으로 설정한다.
		
		switch(paramOp) {
		case "=":
			try {
				int storedV = (Integer)session.getAttribute("value");
				String op = (String)session.getAttribute("op");
				
				int result = 0;
				if(op.equals("+")) {
					result = storedV + paramV;
				} else if (op.equals("-")) {
					result = storedV - paramV;
				} else {
					throw new Exception();
				}
				
				resp.getWriter().println("연산 결과는 " + result + " 입니다.");
			} catch(Exception ignore) {
				resp.getWriter().println("연산식이 올바르지 않습니다.");
			} finally {
				session.removeAttribute("value");
				session.removeAttribute("op");
			}
			break;
		case "+":
		case "-":
			session.setAttribute("value", paramV);
			session.setAttribute("op", paramOp);
			
			resp.sendRedirect("/calc-session.html");
			break;
		}
	}
}
