package com.practice.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/calc-cookie")
public class CalcCookie extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("UTF-8"); // 발신자가 어떻게 보내는지를 지정
		resp.setContentType("text/html; charset=UTF-8"); // 수신자에게 어떻게 처리하라고 지정

		int paramV = 0;
		String paramOp = req.getParameter("op");
		try {
			paramV = Integer.parseInt(req.getParameter("v"));
		} catch(Exception ignore) {}
		
		// Cookie
		// 사용범위: 지정된 path 범위
		// 생명주기: 서버의 생명주기와 무관하다. 만료시간까지, 기본값은 브라우저가 종료될 때 까지 유지된다.
		// 저장위치: 웹 브라우저가 돌아가는 시스템의 메모리 혹은 파일
		// 비고: 브라우저가 쿠키를 허용하지 않으면 무용지물이기 때문에 중요한 기능은 쿠키로 구현하면 안 됨
		// 사용자마다 오래 유지되어야 하는 값을 저장하기에 적합하다.
		
		// 요청자의 쿠키들을 가져온다.
		Cookie[] cookies = req.getCookies();
		
		// 세션의 디폴트 유지시간은 30분이다.
//		session.setMaxInactiveInterval(300) // 세션의 유효시간을 5분으로 설정한다.
		
		switch(paramOp) {
		case "=":
			try {
				int storedV = 0;
				String op = null;
				int flag = 2;
				for(Cookie cookie : cookies) {
					if(cookie.getName().equals("value")) {
						cookie.setMaxAge(0);
						cookie.setPath("/calc-cookie");
						resp.addCookie(cookie);
						storedV = Integer.parseInt(cookie.getValue());
						flag--;
					} else if(cookie.getName().equals("op")) {
						cookie.setMaxAge(0);
						cookie.setPath("/calc-cookie");
						resp.addCookie(cookie);
						op = cookie.getValue();
						flag--;
					}
					if(flag == 0) {
						break;
					}
				}
				
				int result = 0;
				if(op.equals("+")) {
					result = storedV + paramV;
				} else if (op.equals("-")) {
					result = storedV - paramV;
				} else {
					throw new Exception();
				}
				
				resp.getWriter().println("연산 결과는 " + result + " 입니다.");
			} catch(Exception ignore) {
				resp.getWriter().println("연산식이 올바르지 않습니다.");
			}
			break;
		case "+":
		case "-":
			Cookie valueCookie = new Cookie("value", String.valueOf(paramV));
			Cookie opCookie = new Cookie("op", paramOp);
			
//			valueCookie.setPath("/"); // '/'로 설정하면 모든 경로에서 해당 쿠키가 사용됨
			valueCookie.setPath("/calc-cookie");
			opCookie.setPath("/calc-cookie");
			
			// Cookie에 maxAge를 설정하지 않거나 마이너스 값으로 설정하면 세션이 닫힐 때까지 유지된다.
			// 0으로 설정하면 제거된다.
//			valueCookie.setMaxAge(-1);
//			opCookie.setMaxAge(-1);
			
			resp.addCookie(valueCookie);
			resp.addCookie(opCookie);
			
			resp.sendRedirect("/calc-cookie.html");
			break;
		}
	}
}
