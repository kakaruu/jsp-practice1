package com.practice.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// JSP MVC Model 2
// JSP MVC Model 1에서 JSP 문서에 작성했던 컨트롤러를 별도의 java 파일로 분리하는 방법
@WebServlet("/spaghettit-to-jsp-mvc-model2")
public class SpagettiModel2Controller extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Controller 파트
		// JSP 문서 상단에 처리문을 넣어서 화면에 보여줘야하는 데이터들을 Model에다가 넣는다.
		int num = 0;
		try {
			num = Integer.parseInt(request.getParameter("num"));
		} catch (Exception ignore){}
		String model = "";
		if(num <= 0) {
			model = "0보다 큰 정수를 입력해주세요.";
		} else if(num%2 == 0) {
			model = "짝수입니다.";
		} else {
			model = "홀수입니다.";
		}
		
		// redirect와 forward와 비교
		// redirect는 현재 작업했던 내용과 전혀 상관없이 새로운 요청을 하는 기능
		// forward는 현재 작업한 내용을 이어가면서 새로운 요청을 하는 기능
		//
		// forward는 Dispatcher를 통해서 이루어진다.
		// forward를 수행하고 나서도 브라우저에 표시되는 url 주소는 컨트롤러의 주소가 유지된다.
		RequestDispatcher dispatcher
			= request.getRequestDispatcher("/spaghettit-to-jsp-mvc-model2.jsp");
		
		request.setAttribute("model", model);
		
		// 배열 Attribute 테스트
		request.setAttribute("array-or-list", new String[] {"항목1", "항목2"});
		request.setAttribute("arrayOrList", new String[] {"항목1", "항목2"});
		
		// Map Attribute 테스트
		Map map = new HashMap<String, Object>();
		map.put("key1", 1);
		map.put("key2", "문자열");
		request.setAttribute("map", map);
		
		dispatcher.forward(request, response);
	}
}
