package com.practice.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 어노테이션은 클래스에 남기는 주석
// 일반적인 주석은 컴파일 과정에서 무시되지만
// 어노테이션은 무시되지 않고 클래스의 메타데이터를 정의해준다.
// 설정에 따라 어노테이션도 컴파일 과정에서 무시되도록 할 수 있다고 한다.

// WebServlet 어노테이션은 서블릿 3.0 이상부터 사용할 수 있다.
// 이 어노테이션을 사용하려면 web.xml에 metadata-complete를 false로 설정해주어야 한다.
@WebServlet("/hello2")
public class Nana extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html; charset=UTF-8"); // 수신자에게 어떻게 처리하라고 지정
		
		PrintWriter out = resp.getWriter();
		
		int cnt = 100;
		try {
			cnt = Integer.parseInt(req.getParameter("cnt"));
		} catch(Exception ignore) {}
		for(int i=1; i<=cnt; i++) {
			// 크롬은 일반 문서는 텍스트 파일로 인식하기 때문에
			// contentType을 text/html로 설정하지 않으면
			// <br>태그를 텍스트로 그대로 출력한다
			// 반면 익플, 엣지는 기본적으로 html로 인식하기 때문에
			// \n은 먹히지 않고 대신 <br>태그를 인식한다.
			// 개인적으로는 크롬의 방식이 맞는 것 같다.
			out.println(i + "\t안녕하세요<br>"); 
		}
	}
}
